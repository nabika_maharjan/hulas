<?php $uri = explode("/", $this->uri->uri_string()); ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?php $css = (!isset($uri[1])) ? 'class="active"' : ''; ?> 
            <li <?php echo $css;?>>
                <a href="<?php echo site_url('admin'); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?></span>
                </a>
            </li>

            <?php if(control('System', FALSE)):?>
                <?php $css = (isset($uri[1]) && in_array($uri[1], array('users', 'groups', 'permissions'))) ? 'active' : ''; ?>
                <li class="treeview <?php echo $css; ?>">
                    <a href="javascript:void(0)">
                        <i class="fa fa-gear"></i>
                        <span><?php echo lang('menu_system'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                         <?php if(control('Permissions', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'permissions') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/permissions')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_permissions');?></a></li>
                        <?php endif;?>
                        <?php if(control('Groups', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'groups') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/groups')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_groups');?></a></li>
                        <?php endif;?>
                        <?php if(control('Users', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'users') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/users')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_users');?></a></li>
                        <?php endif;?>
                    </ul>
                </li>
            <?php endif;?>
<?php if(control('Permissions', FALSE)):?>
                <?php $css = (isset($uri[1]) && in_array($uri[1], array('categories', 'materials', 'machineries', 'machinery_categories','departments', 'profiles', 'scraps'))) ? 'active' : ''; ?>
                <li class="treeview <?php echo $css; ?>">
                    <a href="javascript:void(0)">
                        <i class="fa fa-files-o"></i>
                        <span><?php echo lang('menu_masters'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if(control('Categories', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'categories') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/categories')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_categories');?></a></li>
                        <?php endif;?>

                         <?php if(control('Materials', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'materials') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/materials')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_materials');?></a></li>
                        <?php endif;?>

                         <?php if(control('Machineries', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'machineries') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/machineries')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_machineries');?></a></li>
                        <?php endif;?>

                        <?php if(control('Machinery Categories', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'machinery_categories') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/machinery_categories')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_machinery_categories');?></a></li>
                        <?php endif;?>

                       

                        <?php if(control('Departments', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'departments') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/departments')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_departments');?></a></li>
                        <?php endif;?>

                        <?php if(control('Profiles', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'profiles') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/profiles')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_profiles');?></a></li>
                        <?php endif;?>

                        <?php if(control('Scraps', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'scraps') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/scraps')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_scraps');?></a></li>
                        <?php endif;?>
                        </ul>
                </li>
            <?php endif;?>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>