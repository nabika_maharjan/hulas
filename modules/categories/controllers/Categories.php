<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Categories
 *
 * Extends the Project_Controller class
 * 
 */

class Categories extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Categories');

        $this->load->model('categories/category_model');
        $this->lang->load('categories/category');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('categories');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'categories';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->category_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->category_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$data['created_at'] = date('Y-m-d');
		
            $success=$this->category_model->insert($data);
        }
        else
        {
        	$data['updated_at'] = date('Y-m-d');
			
            $success=$this->category_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		// $data['created_by'] = $this->input->post('created_by');
		// $data['updated_by'] = $this->input->post('updated_by');
		// $data['deleted_by'] = $this->input->post('deleted_by');
		
		$data['category'] = strtoupper($this->input->post('category'));

        return $data;
   }
   public function get_category(){
		
		
		
		$total=$this->category_model->find_count();
		search_params();
        paging('id');

        search_params();
		 $category= $this->category_model->findAll();
		
		 echo json_encode($category);

   }
    public function delete()
    {
       $data['id'] = $this->input->post('id')[0];
       $this->category_model->delete($data['id']);
     
       // echo $this->db->last_query();
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;      


    }
}