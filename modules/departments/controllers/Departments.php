<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Departments
 *
 * Extends the Project_Controller class
 * 
 */

class Departments extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Departments');

		$this->load->model('departments/department_model');
		$this->lang->load('departments/department'); 
	}

	public function index()
	{
		// Display Page
		

		// print_r($data); exit;
		$data['header'] = lang('departments');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'departments';
		$this->load->view($this->_container,$data);
	}

	public function get_department(){

		$total=$this->department_model->find_count();
		search_params();
		paging('id');

		search_params();
		$category= $this->department_model->findAll();

		echo json_encode($category);

	}

	public function json()
	{
		$this->department_model->_table = 'view_departments';
		search_params();
		
		$total=$this->department_model->find_count(array('deleted_by'=>NULL));
		
		paging('id');
		
		search_params();
		
		$rows=$this->department_model->findAll(array('deleted_by'=>NULL));
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function combo_json_parent()
    {
		// $this->department_model->_table = 'mst_department';

        $rows = $this->department_model->findAll(array('parent_id'=>0),array('id','name'));
        // $rows = $this->stock_yard_model->findAll(null,array('id','name'));
        echo json_encode($rows);
    }
    public function delete()
    {
       $data['id'] = $this->input->post('id')[0];
       $this->department_model->delete($data['id']);
       
       
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;
    }

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $data['created_at'] = date('Y-m-d');
        	$success=$this->department_model->insert($data);
        }
        else
        {
            $data['updated_at'] = date('Y-m-d');
            $data['deleted_at'] = date('Y-m-d');
        	$success=$this->department_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }


        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    	}
    	// $data['created_by'] = $this->input->post('created_by');
    	// $data['updated_by'] = $this->input->post('updated_by');
    	// $data['deleted_by'] = $this->input->post('deleted_by');
    	// $data['created_at'] = $this->input->post('created_at');
    	// $data['updated_at'] = $this->input->post('updated_at');
    	// $data['deleted_at'] = $this->input->post('deleted_at');
    	$data['name'] = strtoupper($this->input->post('name'));
    	$data['parent_id'] = $this->input->post('parent_id');
    	if(!$data['parent_id'])
    	{
    		$data['parent_id'] = 0;
    	}

    	return $data;
    }
    
}