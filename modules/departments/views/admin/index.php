<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('departments'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('departments'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridDepartmentToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDepartmentInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDepartmentFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridDepartment"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowDepartment">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-departments', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "departments_id"/>
            <table class="form-table">

				<tr>
					<td><label for='departments_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></td>
					<td><input id='departments_name' class='text_input' name='name'></td>
				</tr>
				<tr>
					<td><label for='department'>Department Type</label></td>
					<td><div  name="parent_id" id="parent_id"></div></td>
				</tr>
			
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDepartmentSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDepartmentCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var departmentsDataSource =
	{
		datatype: "json",
		datafields: [

			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			{ name: 'parent_name', type: 'string' },
			{ name: 'parent_id', type: 'number' },

			
        ],
		url: '<?php echo site_url("admin/departments/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	departmentsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridDepartment").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridDepartment").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridDepartment").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: departmentsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridDepartmentToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editDepartmentRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					var d = '<a href="javascript:void(0)" onclick="deleteDepartmentRecord(' + index + '); return false;" title="Delete"><i class="fa fa-trash"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e +'&nbsp;'+ d+'</div>';
				}
			},
			// { text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Parent',datafield: 'parent_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridDepartment").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridDepartmentFilterClear', function () { 
		$('#jqxGridDepartment').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridDepartmentInsert', function () { 
		openPopupWindow('jqxPopupWindowDepartment', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowDepartment").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowDepartment").on('close', function () {
        reset_form_departments();
    });

    $("#jqxDepartmentCancelButton").on('click', function () {
        reset_form_departments();
        $('#jqxPopupWindowDepartment').jqxWindow('close');
    });

    /*$('#form-departments').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#departments_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#departments_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#parent_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#parent_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxDepartmentSubmitButton").on('click', function () {
        saveDepartmentRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveDepartmentRecord();
                }
            };
        $('#form-departments').jqxValidator('validate', validationResult);
        */
    });
});

function editDepartmentRecord(index){
	// exit;
    var row =  $("#jqxGridDepartment").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#departments_id').val(row.id);
     	$('#departments_name').val(row.name);
		$('#parent_id').jqxNumberInput('val', row.parent_id);
		
        openPopupWindow('jqxPopupWindowDepartment', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}
function deleteDepartmentRecord(index)
{
	var row =  $("#jqxGridDepartment").jqxGrid('getrowdata', index);
	// console.log(row);
	if(confirm("Are you sure want to delete!!!") == true){
		$.post("<?php   echo site_url('admin/departments/delete')?>", {id:[row.uid]}, function(result){
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_departments();
				$('#jqxGridDepartment').jqxGrid('updatebounddata');
				$.notify("Successfully Deleted",{  
                 	className:'success',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
			}
			else
			{
				$.notify("Failed",{  
                 	className:'error',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
			}
			
		});

	}

}
function saveDepartmentRecord(){
	var data = $("#form-departments").serialize();
	
	$('#jqxPopupWindowDepartment').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/departments/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_departments();
                $('#jqxGridDepartment').jqxGrid('updatebounddata');
                $('#jqxPopupWindowDepartment').jqxWindow('close');
                $.notify("Success",{  
                 	className:'success',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
            }
            else{
            	$.notify("Failed",{  
                 	className:'error',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
            }
            $('#jqxPopupWindowDepartment').unblock();
        }
    });
}

function reset_form_departments(){
	$('#departments_id').val('');
    $('#form-departments')[0].reset();
}
</script>

<script type="text/javascript">
	$(document).ready(function(){
		var parent_data_source = {
	        url: '<?php echo site_url("admin/Departments/combo_json_parent"); ?>',
	        datatype: 'json',
	        datafields: [
	        {name: 'id', type: 'number'},
	        {name: 'name', type: 'string'},
	        ]
	    };

        parent_data_source = new $.jqx.dataAdapter(parent_data_source);

	    $("#parent_id").jqxComboBox({ 
	        source: parent_data_source, 
	        width: '200px', 
	        height: '25px',
	        // placeHolder:'Select month',
	        displayMember: "name",
	        valueMember: "id",
	        // size: 10,

	    })
	});
</script>

