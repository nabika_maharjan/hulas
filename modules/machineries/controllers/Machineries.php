<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Machineries
 *
 * Extends the Project_Controller class
 * 
 */

class Machineries extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Machineries');

        $this->load->model('machineries/machinery_model');
        $this->lang->load('machineries/machinery');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('machineries');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'machineries';
		$this->load->view($this->_container,$data);
	}

	public function json()

	{
		$this->machinery_model->_table = 'view_machinery';
		search_params();
		
		$total=$this->machinery_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->machinery_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;

		
	}
    public function delete()
    {
       $data['id'] = $this->input->post('id')[0];
       $this->machinery_model->delete($data['id']);
       
       
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;
    }
	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->machinery_model->insert($data);
        }
        else
        {
            $success=$this->machinery_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['machinery_category_id'] = $this->input->post('machinery_category_id');
		$data['machine_code'] = $this->input->post('machine_code');
		$data['department_id'] = $this->input->post('department_id');

        return $data;
   }

   


}