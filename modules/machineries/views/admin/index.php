<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('machineries'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('machineries'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridMachineryToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridMachineryInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridMachineryFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridMachinery"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowMachinery">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-machineries', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "machineries_id"/>
            <table class="form-table">
				<!-- <tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><input id='created_at' class='text_input' name='created_at'></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><input id='updated_at' class='text_input' name='updated_at'></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><input id='deleted_at' class='text_input' name='deleted_at'></td>
				</tr> -->
				<tr>
					<td><label for='machinery_category_id'><?php echo lang('machinery_category_id')?></label></td>
					<td><div id='machinery_category_id' class='number_general' name='machinery_category_id'></div></td>
				</tr>
				<tr>
					<td><label for='machine_code'><?php echo lang('machine_code')?></label></td>
					<td><input id='machine_code' class='text_input' name='machine_code'></td>
				</tr>
				<tr>
					<td><label for='department_id'><?php echo lang('department_id')?></label></td>
					<td><div id='department_id' class='number_general' name='department_id'></div></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxMachinerySubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxMachineryCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){
	 var departmentAdapter;
	 var categoryAdapter;
	var machineriesDataSource =
	{
		datatype: "json",
		datafields: [
			 { name: 'id', type: 'number' },
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'string' },
			// { name: 'updated_at', type: 'string' },
			// { name: 'deleted_at', type: 'string' },
			{ name: 'machinery_category_id', type: 'number' },
			{ name: 'machine_code', type: 'string' },
			//{ name: 'department_id', type: 'number' },
			{ name: 'department_name', type: 'string' },
			{ name: 'machinery_code', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/machineries/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	machineriesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridMachinery").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridMachinery").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridMachinery").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: machineriesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridMachineryToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editMachineryRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
			
					var d = '<a href="javascript:void(0)" onclick="deleteMachineryRecord(' + index + '); return false;" title="Delete"><i class="fa fa-trash"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e +'&nbsp;'+ d+'</div>';
				}
			},
			//{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("machinery_category_id"); ?>',datafield: 'machinery_category_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("machine_code"); ?>',datafield: 'machine_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Department Name',datafield: 'department_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Machinery Code',datafield: 'machinery_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridMachinery").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridMachineryFilterClear', function () { 
		$('#jqxGridMachinery').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridMachineryInsert', function () { 
		openPopupWindow('jqxPopupWindowMachinery', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowMachinery").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowMachinery").on('close', function () {
        reset_form_machineries();
    });

    $("#jqxMachineryCancelButton").on('click', function () {
        reset_form_machineries();
        $('#jqxPopupWindowMachinery').jqxWindow('close');
    });

    /*$('#form-machineries').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#machinery_category_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#machinery_category_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#machine_code', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#machine_code').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#department_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#department_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxMachinerySubmitButton").on('click', function () {
        saveMachineryRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveMachineryRecord();
                }
            };
        $('#form-machineries').jqxValidator('validate', validationResult);
        */
    });

    var categorycount =
		{
			datatype: "json",
			datafields: [
				{ name: 'id',type: 'string'},
                { name: 'name',type: 'string'},
                
			],
			url: '<?php echo site_url('admin/machinery_categories/get_category')?>'
		};

		 categoryAdapter = new $.jqx.dataAdapter(categorycount);

		 $("#machinery_category_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: categoryAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		 var departmentcount =
		{
			datatype: "json",
			datafields: [
				{ name: 'id',type: 'string'},
                { name: 'name',type: 'string'},
                
			],
			url: '<?php echo site_url('admin/departments/get_department')?>'
		};

		 departmentAdapter = new $.jqx.dataAdapter(departmentcount);

		 $("#department_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: departmentAdapter,
			displayMember: "name",
			valueMember: "id",
		});
});

function editMachineryRecord(index){
    var row =  $("#jqxGridMachinery").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#machineries_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
		// $('#updated_by').jqxNumberInput('val', row.updated_by);
		// $('#deleted_by').jqxNumberInput('val', row.deleted_by);
		// $('#created_at').val(row.created_at);
		// $('#updated_at').val(row.updated_at);
		// $('#deleted_at').val(row.deleted_at);
		$('#machinery_category_id').jqxNumberInput('val', row.machinery_category_id);
		$('#machine_code').val(row.machine_code);
		$('#department_id').jqxNumberInput('val', row.department_id);
		
        openPopupWindow('jqxPopupWindowMachinery', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}
function deleteMachineryRecord(index)
{
	var row =  $("#jqxGridMachinery").jqxGrid('getrowdata', index);
	// console.log(row);
	if(confirm("Are you sure want to delete!!!") == true){
		$.post("<?php   echo site_url('admin/machineries/delete')?>", {id:[row.id]}, function(result){
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_machineries();
				$('#jqxGridMachinery').jqxGrid('updatebounddata');
				$.notify("Successfully Deleted",{  
                 	className:'success',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
			}
			else
			{
				$.notify("Failed",{  
                 	className:'error',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
			}
			
		});

	}

}
function saveMachineryRecord(){
    var data = $("#form-machineries").serialize();
	
	$('#jqxPopupWindowMachinery').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/machineries/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_machineries();
                $('#jqxGridMachinery').jqxGrid('updatebounddata');
                $('#jqxPopupWindowMachinery').jqxWindow('close');
                 $.notify("Success",{  
                 	className:'success',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
            }
            else{
            	$.notify("Failed",{  
                 	className:'error',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
            }
            $('#jqxPopupWindowMachinery').unblock();
        }
    });
}

function reset_form_machineries(){
	$('#machineries_id').val('');
    $('#form-machineries')[0].reset();
}
</script>