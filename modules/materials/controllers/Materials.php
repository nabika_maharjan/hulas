<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Materials
 *
 * Extends the Project_Controller class
 * 
 */

class Materials extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Materials');

        $this->load->model('materials/material_model');
        $this->lang->load('materials/material');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('materials');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'materials';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->material_model->_table = 'view_material';
		search_params();
		
		$total=$this->material_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->material_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}
	public function delete()
    {
       $data['id'] = $this->input->post('id')[0];
       $this->material_model->delete($data['id']);
     
       // echo $this->db->last_query();
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;      


    }

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->material_model->insert($data);
        }
        else
        {
            $success=$this->material_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['name'] = strtoupper($this->input->post('name'));
		$data['category_id'] = $this->input->post('category_id');

        return $data;
   }
}