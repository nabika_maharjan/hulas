<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('materials'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('materials'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridMaterialToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridMaterialInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridMaterialFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridMaterial"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowMaterial">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-materials', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "materials_id"/>
            <table class="form-table">
				<!-- <tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><input id='created_at' class='text_input' name='created_at'></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><input id='updated_at' class='text_input' name='updated_at'></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><input id='deleted_at' class='text_input' name='deleted_at'></td>
				</tr> -->
				<tr>
					<td><label for='materials_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></td>
					<td><input id='materials_name' class='text_input' name='name'></td>
				</tr>
				<tr>
					<td><label for='category_id'><?php echo lang('category_id')?></label></td>
					<td><div id='category_id' class='number_general' name='category_id'></div></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxMaterialSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxMaterialCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){
var categoryAdapter;
	var materialsDataSource =
	{
		datatype: "json",
		datafields: [
		 { name: 'id', type: 'number' },
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'string' },
			// { name: 'updated_at', type: 'string' },
			// { name: 'deleted_at', type: 'string' },
			{ name: 'name', type: 'string' },
			{ name: 'category', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/materials/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	materialsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridMaterial").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridMaterial").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridMaterial").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: materialsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridMaterialToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editMaterialRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					var d = '<a href="javascript:void(0)" onclick="deleteMaterialRecord(' + index + '); return false;" title="Delete"><i class="fa fa-trash"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e +'&nbsp;'+ d+'</div>';
				}
			},
			//{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Category',datafield: 'category',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridMaterial").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridMaterialFilterClear', function () { 
		$('#jqxGridMaterial').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridMaterialInsert', function () { 
		openPopupWindow('jqxPopupWindowMaterial', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowMaterial").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowMaterial").on('close', function () {
        reset_form_materials();
    });

    $("#jqxMaterialCancelButton").on('click', function () {
        reset_form_materials();
        $('#jqxPopupWindowMaterial').jqxWindow('close');
    });

    /*$('#form-materials').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#materials_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#materials_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#category_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#category_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxMaterialSubmitButton").on('click', function () {
        saveMaterialRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveMaterialRecord();
                }
            };
        $('#form-materials').jqxValidator('validate', validationResult);
        */
    });
    var categorycount =
		{
			datatype: "json",
			datafields: [
				{ name: 'id',type: 'string'},
                { name: 'category',type: 'string'},
                
			],
			url: '<?php echo site_url('admin/categories/get_category')?>'
		};

		 categoryAdapter = new $.jqx.dataAdapter(categorycount);

		 $("#category_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: categoryAdapter,
			displayMember: "category",
			valueMember: "id",
		});
});

function editMaterialRecord(index){
    var row =  $("#jqxGridMaterial").jqxGrid('getrowdata', index);
    console.log(row);
  	if (row) {
  		$('#materials_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
		// $('#updated_by').jqxNumberInput('val', row.updated_by);
		// $('#deleted_by').jqxNumberInput('val', row.deleted_by);
		// $('#created_at').val(row.created_at);
		// $('#updated_at').val(row.updated_at);
		// $('#deleted_at').val(row.deleted_at);
		$('#materials_name').val(row.name);
		$('#category_id').jqxNumberInput('val', row.category_id);
		
        openPopupWindow('jqxPopupWindowMaterial', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}
function deleteMaterialRecord(index)
{
	var row =  $("#jqxGridMaterial").jqxGrid('getrowdata', index);
	// console.log(row);
	if(confirm("Are you sure want to delete!!!") == true){
		$.post("<?php   echo site_url('admin/materials/delete')?>", {id:[row.id]}, function(result){
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_materials();
				$('#jqxGridMaterial').jqxGrid('updatebounddata');
				$.notify("Successfully Deleted",{  
                 	className:'success',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
			}
			else
			{
				$.notify("Failed",{  
                 	className:'error',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
			}
			
		});

	}

}
function saveMaterialRecord(){
    var data = $("#form-materials").serialize();
	
	$('#jqxPopupWindowMaterial').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/materials/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_materials();
                $('#jqxGridMaterial').jqxGrid('updatebounddata');
                $('#jqxPopupWindowMaterial').jqxWindow('close');
                $.notify("Success",{  
                 	className:'success',
                    clickToHide: false,
                    globalPosition: 'right bottom',
                });
            }
            else{
            	$.notify("Failed",{  
	                 	className:'error',
	                    clickToHide: false,
	                    globalPosition: 'right bottom',
	            });
            }
            $('#jqxPopupWindowMaterial').unblock();
        }
    });
}

function reset_form_materials(){
	$('#materials_id').val('');
    $('#form-materials')[0].reset();
}
</script>