<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['user_id'] = 'User Id';
$lang['first_name'] = 'First Name';
$lang['middle_name'] = 'Middle Name';
$lang['last_name'] = 'Last Name';
$lang['address'] = 'Address';
$lang['contact_number'] = 'Contact Number';
$lang['contact_office'] = 'Contact Office';
$lang['mobile'] = 'Mobile';
$lang['email'] = 'Email';
$lang['department_id'] = 'Department';
$lang['group_id'] = 'Group';

$lang['user_details']='User Details';