<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('user_details'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('user_details'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridUser_detailToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridUser_detailInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridUser_detailFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridUser_detail"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowUser_detail">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-user_details', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "user_details_id"/>
            <table class="form-table">
				<!-- <tr>
					<td><label for='user_id'><?php echo lang('user_id')?></label></td>
					<td><div id='user_id' class='number_general' name='user_id'></div></td>
				</tr> -->
				<tr>
					<td><label for='first_name'><?php echo lang('first_name')?></label></td>
					<td><input id='first_name' class='text_input' name='first_name'></td>
				</tr>
				<tr>
					<td><label for='middle_name'><?php echo lang('middle_name')?></label></td>
					<td><input id='middle_name' class='text_input' name='middle_name'></td>
				</tr>
				<tr>
					<td><label for='last_name'><?php echo lang('last_name')?></label></td>
					<td><input id='last_name' class='text_input' name='last_name'></td>
				</tr>
				<tr>
					<td><label for='address'><?php echo lang('address')?></label></td>
					<td><input id='address' class='text_input' name='address'></td>
				</tr>
				<tr>
					<td><label for='contact_number'><?php echo lang('contact_number')?></label></td>
					<td><input id='contact_number' class='text_input' name='contact_number'></td>
				</tr>
				<tr>
					<td><label for='contact_office'><?php echo lang('contact_office')?></label></td>
					<td><input id='contact_office' class='text_input' name='contact_office'></td>
				</tr>
				<tr>
					<td><label for='mobile'><?php echo lang('mobile')?></label></td>
					<td><input id='mobile' class='text_input' name='mobile'></td>
				</tr>
				<tr>
					<td><label for='email'><?php echo lang('email')?></label></td>
					<td><input id='email' class='text_input' name='email'></td>
				</tr>
				<tr>
					<td><label for='department_id'><?php echo lang('department_id')?></label></td>
					<td><div id='department_id' class='number_general' name='department_id'></div></td>
				</tr>
				<tr>
					<td><label for='group_id'><?php echo lang('group_id')?></label></td>
					<td><div id='group_id' class='number_general' name='group_id'></div></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxUser_detailSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxUser_detailCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){
	// departments
    var departmentDataSource = {
       	url : '<?php echo site_url("admin/departments/get_department"); ?>',
       	datatype: 'json',
       	datafields: [
       	{ name: 'id', type: 'number' },
       	{ name: 'name', type: 'string' },
       	],
       	async: false,
       	cache: true
   	}
   	departmentDataAdapter = new $.jqx.dataAdapter(departmentDataSource);

   	$("#department_id").jqxComboBox({
       	theme: theme,
       	width: 195,
       	height: 25,
       	selectionMode: 'dropDownList',
       	autoComplete: true,
       	searchMode: 'containsignorecase',
       	source: departmentDataAdapter,
       	displayMember: "name",
       	valueMember: "id",
   	});

   	// departments
    var groupDataSource = {
       	url : '<?php echo site_url("admin/departments/get_department"); ?>',
       	datatype: 'json',
       	datafields: [
       	{ name: 'id', type: 'number' },
       	{ name: 'name', type: 'string' },
       	],
       	async: false,
       	cache: true
   	}
   	groupDataAdapter = new $.jqx.dataAdapter(groupDataSource);

   	$("#group_id").jqxComboBox({
       	theme: theme,
       	width: 195,
       	height: 25,
       	selectionMode: 'dropDownList',
       	autoComplete: true,
       	searchMode: 'containsignorecase',
       	source: groupDataAdapter,
       	displayMember: "name",
       	valueMember: "id",
   	});


	var user_detailsDataSource =
	{

		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'string' },
			{ name: 'updated_at', type: 'string' },
			{ name: 'deleted_at', type: 'string' },
			{ name: 'user_id', type: 'number' },
			{ name: 'first_name', type: 'string' },
			{ name: 'middle_name', type: 'string' },
			{ name: 'last_name', type: 'string' },
			{ name: 'address', type: 'string' },
			{ name: 'contact_number', type: 'string' },
			{ name: 'contact_office', type: 'string' },
			{ name: 'mobile', type: 'string' },
			{ name: 'email', type: 'string' },
			{ name: 'department_id', type: 'number' },
			
        ],
		url: '<?php echo site_url("admin/user_details/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	user_detailsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridUser_detail").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridUser_detail").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridUser_detail").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: user_detailsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridUser_detailToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editUser_detailRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			
			{ text: '<?php echo lang("user_id"); ?>',datafield: 'user_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("first_name"); ?>',datafield: 'first_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("middle_name"); ?>',datafield: 'middle_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("last_name"); ?>',datafield: 'last_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("address"); ?>',datafield: 'address',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("contact_number"); ?>',datafield: 'contact_number',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("contact_office"); ?>',datafield: 'contact_office',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("mobile"); ?>',datafield: 'mobile',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("email"); ?>',datafield: 'email',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("department_id"); ?>',datafield: 'department_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridUser_detail").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridUser_detailFilterClear', function () { 
		$('#jqxGridUser_detail').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridUser_detailInsert', function () { 
		openPopupWindow('jqxPopupWindowUser_detail', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowUser_detail").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowUser_detail").on('close', function () {
        reset_form_user_details();
    });

    $("#jqxUser_detailCancelButton").on('click', function () {
        reset_form_user_details();
        $('#jqxPopupWindowUser_detail').jqxWindow('close');
    });

    $('#form-user_details').jqxValidator({
        // hintType: 'label',
        animationDuration: 500,
        rules: [

			// { input: '#user_id', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#user_id').jqxNumberInput('val');
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },

			{ input: '#first_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#first_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#middle_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#middle_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#last_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#last_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#address', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#address').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#contact_number', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#contact_number').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#contact_office', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#contact_office').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#mobile', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#mobile').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#email', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#email').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#department_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#department_id').jqxComboBox('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
			{ input: '#group_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#group_id').jqxComboBox('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });

    $("#jqxUser_detailSubmitButton").on('click', function () {
        // saveUser_detailRecord();
        
        var validationResult = function (isValid) {
                if (isValid) {
                   saveUser_detailRecord();
                }
            };
        $('#form-user_details').jqxValidator('validate', validationResult);
        
    });
});

function editUser_detailRecord(index){
    var row =  $("#jqxGridUser_detail").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#user_details_id').val(row.id);
        $('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#user_id').jqxNumberInput('val', row.user_id);
		$('#first_name').val(row.first_name);
		$('#middle_name').val(row.middle_name);
		$('#last_name').val(row.last_name);
		$('#address').val(row.address);
		$('#contact_number').val(row.contact_number);
		$('#contact_office').val(row.contact_office);
		$('#mobile').val(row.mobile);
		$('#email').val(row.email);
		$('#department_id').jqxNumberInput('val', row.department_id);
		$('#group_id').jqxNumberInput('val', row.group_id);
		$('#group_name').jqxNumberInput('val', row.group_name);
		
        openPopupWindow('jqxPopupWindowUser_detail', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveUser_detailRecord(){
    var data = $("#form-user_details").serialize();
	
	$('#jqxPopupWindowUser_detail').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/user_details/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_user_details();
                $('#jqxGridUser_detail').jqxGrid('updatebounddata');
                $('#jqxPopupWindowUser_detail').jqxWindow('close');
            }
            $('#jqxPopupWindowUser_detail').unblock();
        }
    });
}

function reset_form_user_details(){
	$('#user_details_id').val('');
    $('#form-user_details')[0].reset();
}
</script>