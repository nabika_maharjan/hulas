/*
Navicat PGSQL Data Transfer

Source Server         : postgres
Source Server Version : 90405
Source Host           : localhost:5432
Source Database       : hulas
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90405
File Encoding         : 65001

Date: 2017-10-06 09:42:47
*/


-- ----------------------------
-- Sequence structure for aauth_groups_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."aauth_groups_id_seq";
CREATE SEQUENCE "public"."aauth_groups_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1001
 CACHE 1;

-- ----------------------------
-- Sequence structure for aauth_login_attempts_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."aauth_login_attempts_id_seq";
CREATE SEQUENCE "public"."aauth_login_attempts_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."aauth_login_attempts_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for aauth_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."aauth_permissions_id_seq";
CREATE SEQUENCE "public"."aauth_permissions_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1006
 CACHE 1;
SELECT setval('"public"."aauth_permissions_id_seq"', 1006, true);

-- ----------------------------
-- Sequence structure for aauth_pms_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."aauth_pms_id_seq";
CREATE SEQUENCE "public"."aauth_pms_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for aauth_user_variables_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."aauth_user_variables_id_seq";
CREATE SEQUENCE "public"."aauth_user_variables_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for aauth_users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."aauth_users_id_seq";
CREATE SEQUENCE "public"."aauth_users_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1001
 CACHE 1;

-- ----------------------------
-- Sequence structure for mst_category_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mst_category_id_seq";
CREATE SEQUENCE "public"."mst_category_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."mst_category_id_seq"', 3, true);

-- ----------------------------
-- Sequence structure for mst_department_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mst_department_id_seq";
CREATE SEQUENCE "public"."mst_department_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for mst_machinery_category_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mst_machinery_category_id_seq";
CREATE SEQUENCE "public"."mst_machinery_category_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for mst_machinery_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mst_machinery_id_seq";
CREATE SEQUENCE "public"."mst_machinery_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for mst_material_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mst_material_id_seq";
CREATE SEQUENCE "public"."mst_material_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."mst_material_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for mst_profile_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mst_profile_id_seq";
CREATE SEQUENCE "public"."mst_profile_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for mst_scrap_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mst_scrap_id_seq";
CREATE SEQUENCE "public"."mst_scrap_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_activity_logs_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_activity_logs_id_seq";
CREATE SEQUENCE "public"."project_activity_logs_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."project_activity_logs_id_seq"', 4, true);

-- ----------------------------
-- Sequence structure for project_audit_logs_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_audit_logs_id_seq";
CREATE SEQUENCE "public"."project_audit_logs_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Table structure for aauth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_group_permissions";
CREATE TABLE "public"."aauth_group_permissions" (
"perm_id" int4 NOT NULL,
"group_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_group_permissions
-- ----------------------------
INSERT INTO "public"."aauth_group_permissions" VALUES ('1', '1');
INSERT INTO "public"."aauth_group_permissions" VALUES ('5', '100');

-- ----------------------------
-- Table structure for aauth_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_groups";
CREATE TABLE "public"."aauth_groups" (
"id" int4 DEFAULT nextval('aauth_groups_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default",
"definition" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_groups
-- ----------------------------
INSERT INTO "public"."aauth_groups" VALUES ('1', 'Member', 'Member Default Access Group');
INSERT INTO "public"."aauth_groups" VALUES ('2', 'Super Administrator', 'Super Administrator Access Group');
INSERT INTO "public"."aauth_groups" VALUES ('100', 'Administrator', 'Administrator Access Group');

-- ----------------------------
-- Table structure for aauth_login_attempts
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_login_attempts";
CREATE TABLE "public"."aauth_login_attempts" (
"id" int4 DEFAULT nextval('aauth_login_attempts_id_seq'::regclass) NOT NULL,
"ip_address" varchar(40) COLLATE "default" DEFAULT 0 NOT NULL,
"timestamp" timestamp(6),
"login_attempts" int4 DEFAULT 0 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_permissions";
CREATE TABLE "public"."aauth_permissions" (
"id" int4 DEFAULT nextval('aauth_permissions_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default",
"definition" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_permissions
-- ----------------------------
INSERT INTO "public"."aauth_permissions" VALUES ('1', 'Control Panel', 'Control Panel');
INSERT INTO "public"."aauth_permissions" VALUES ('2', 'System', 'System');
INSERT INTO "public"."aauth_permissions" VALUES ('3', 'Users', 'Users');
INSERT INTO "public"."aauth_permissions" VALUES ('4', 'Groups', 'Groups');
INSERT INTO "public"."aauth_permissions" VALUES ('5', 'Permissions', 'Permissions');
INSERT INTO "public"."aauth_permissions" VALUES ('6', 'Categories', 'Categories');
INSERT INTO "public"."aauth_permissions" VALUES ('1001', 'Departments', 'Departments');
INSERT INTO "public"."aauth_permissions" VALUES ('1002', 'Machineries', 'Machineries');
INSERT INTO "public"."aauth_permissions" VALUES ('1003', 'Machinery Categories', 'Machinery Categories');
INSERT INTO "public"."aauth_permissions" VALUES ('1004', 'Materials', 'Materials');
INSERT INTO "public"."aauth_permissions" VALUES ('1005', 'Profiles', 'Profiles');
INSERT INTO "public"."aauth_permissions" VALUES ('1006', 'Scraps', 'Scraps');

-- ----------------------------
-- Table structure for aauth_pms
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_pms";
CREATE TABLE "public"."aauth_pms" (
"id" int4 DEFAULT nextval('aauth_pms_id_seq'::regclass) NOT NULL,
"sender_id" int4 NOT NULL,
"receiver_id" int4 NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"message" text COLLATE "default",
"date_sent" timestamp(6),
"date_read" timestamp(6),
"pm_deleted_sender" int4 DEFAULT 0 NOT NULL,
"pm_deleted_receiver" int4 DEFAULT 0 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_pms
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_sub_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_sub_groups";
CREATE TABLE "public"."aauth_sub_groups" (
"group_id" int4 NOT NULL,
"subgroup_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_sub_groups
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_user_groups";
CREATE TABLE "public"."aauth_user_groups" (
"user_id" int4 NOT NULL,
"group_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_user_groups
-- ----------------------------
INSERT INTO "public"."aauth_user_groups" VALUES ('1', '1');
INSERT INTO "public"."aauth_user_groups" VALUES ('1', '2');
INSERT INTO "public"."aauth_user_groups" VALUES ('2', '1');
INSERT INTO "public"."aauth_user_groups" VALUES ('2', '100');

-- ----------------------------
-- Table structure for aauth_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_user_permissions";
CREATE TABLE "public"."aauth_user_permissions" (
"perm_id" int4 NOT NULL,
"user_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_user_permissions
-- ----------------------------
INSERT INTO "public"."aauth_user_permissions" VALUES ('6', '2');
INSERT INTO "public"."aauth_user_permissions" VALUES ('1001', '2');
INSERT INTO "public"."aauth_user_permissions" VALUES ('1002', '2');
INSERT INTO "public"."aauth_user_permissions" VALUES ('1003', '2');
INSERT INTO "public"."aauth_user_permissions" VALUES ('1004', '2');
INSERT INTO "public"."aauth_user_permissions" VALUES ('1005', '2');
INSERT INTO "public"."aauth_user_permissions" VALUES ('1006', '2');

-- ----------------------------
-- Table structure for aauth_user_variables
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_user_variables";
CREATE TABLE "public"."aauth_user_variables" (
"id" int4 DEFAULT nextval('aauth_user_variables_id_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"data_key" varchar(100) COLLATE "default" NOT NULL,
"value" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_user_variables
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."aauth_users";
CREATE TABLE "public"."aauth_users" (
"id" int4 DEFAULT nextval('aauth_users_id_seq'::regclass) NOT NULL,
"email" varchar(100) COLLATE "default" NOT NULL,
"pass" varchar(255) COLLATE "default" NOT NULL,
"username" varchar(100) COLLATE "default",
"fullname" varchar(255) COLLATE "default",
"banned" int4 DEFAULT 0 NOT NULL,
"last_login" timestamp(6),
"last_activity" timestamp(6),
"date_created" timestamp(6),
"forgot_exp" text COLLATE "default",
"remember_time" timestamp(6),
"remember_exp" text COLLATE "default",
"verification_code" text COLLATE "default",
"totp_secret" varchar(16) COLLATE "default" DEFAULT NULL::character varying,
"ip_address" varchar(40) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of aauth_users
-- ----------------------------
INSERT INTO "public"."aauth_users" VALUES ('1', 'superadmin@gmail.com', '8b235284a9f7a82364468e52dab386f33844421b481113794e0b4d634c86d0f3', 'superadmin', 'Super Administrator', '0', '2017-10-05 16:49:55', '2017-10-05 17:59:23', '2017-10-05 16:33:54', null, null, null, null, null, '::1');
INSERT INTO "public"."aauth_users" VALUES ('2', 'admin@no.com', '52b3a93aac36bd14b3a1c9e7118f79981d14d39c6fd5118884d7544e58232a8d', 'admin', 'Administrator', '0', '2017-10-05 16:35:07', '2017-10-05 16:49:15', '2017-10-05 16:33:54', null, null, null, null, null, '::1');

-- ----------------------------
-- Table structure for mst_category
-- ----------------------------
DROP TABLE IF EXISTS "public"."mst_category";
CREATE TABLE "public"."mst_category" (
"id" int4 DEFAULT nextval('mst_category_id_seq'::regclass) NOT NULL,
"created_by" int4,
"updated_by" int4,
"deleted_by" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6),
"category" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of mst_category
-- ----------------------------
INSERT INTO "public"."mst_category" VALUES ('3', '1', '1', null, '2017-10-05 17:44:16', '2017-10-05 17:44:16', null, 'Raw Material');

-- ----------------------------
-- Table structure for mst_department
-- ----------------------------
DROP TABLE IF EXISTS "public"."mst_department";
CREATE TABLE "public"."mst_department" (
"id" int4 DEFAULT nextval('mst_department_id_seq'::regclass) NOT NULL,
"created_by" int4,
"updated_by" int4,
"deleted_by" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6),
"name" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of mst_department
-- ----------------------------

-- ----------------------------
-- Table structure for mst_machinery
-- ----------------------------
DROP TABLE IF EXISTS "public"."mst_machinery";
CREATE TABLE "public"."mst_machinery" (
"id" int4 DEFAULT nextval('mst_machinery_id_seq'::regclass) NOT NULL,
"created_by" int4,
"updated_by" int4,
"deleted_by" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6),
"machinery_category_id" int4,
"machine_code" varchar(255) COLLATE "default",
"department_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of mst_machinery
-- ----------------------------

-- ----------------------------
-- Table structure for mst_machinery_category
-- ----------------------------
DROP TABLE IF EXISTS "public"."mst_machinery_category";
CREATE TABLE "public"."mst_machinery_category" (
"id" int4 DEFAULT nextval('mst_machinery_category_id_seq'::regclass) NOT NULL,
"created_by" int4,
"updated_by" int4,
"deleted_by" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6),
"name" varchar(255) COLLATE "default",
"machinery_code" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of mst_machinery_category
-- ----------------------------

-- ----------------------------
-- Table structure for mst_material
-- ----------------------------
DROP TABLE IF EXISTS "public"."mst_material";
CREATE TABLE "public"."mst_material" (
"id" int4 DEFAULT nextval('mst_material_id_seq'::regclass) NOT NULL,
"created_by" int4,
"updated_by" int4,
"deleted_by" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6),
"name" varchar(255) COLLATE "default",
"category_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of mst_material
-- ----------------------------
INSERT INTO "public"."mst_material" VALUES ('1', '1', '1', null, '2017-10-05 17:59:21', '2017-10-05 17:59:21', null, 'test', '3');

-- ----------------------------
-- Table structure for mst_profile
-- ----------------------------
DROP TABLE IF EXISTS "public"."mst_profile";
CREATE TABLE "public"."mst_profile" (
"id" int4 DEFAULT nextval('mst_profile_id_seq'::regclass) NOT NULL,
"created_by" int4,
"updated_by" int4,
"deleted_by" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6),
"name" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of mst_profile
-- ----------------------------

-- ----------------------------
-- Table structure for mst_scrap
-- ----------------------------
DROP TABLE IF EXISTS "public"."mst_scrap";
CREATE TABLE "public"."mst_scrap" (
"id" int4 DEFAULT nextval('mst_scrap_id_seq'::regclass) NOT NULL,
"created_by" int4,
"updated_by" int4,
"deleted_by" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6),
"name" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of mst_scrap
-- ----------------------------

-- ----------------------------
-- Table structure for project_activity_logs
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_activity_logs";
CREATE TABLE "public"."project_activity_logs" (
"id" int4 DEFAULT nextval('project_activity_logs_id_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"table_name" varchar(255) COLLATE "default",
"table_pk" int4 NOT NULL,
"action" varchar(255) COLLATE "default",
"action_dttime" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of project_activity_logs
-- ----------------------------
INSERT INTO "public"."project_activity_logs" VALUES ('1', '1', 'mst_category', '1', 'insert', '2017-10-05 17:30:16');
INSERT INTO "public"."project_activity_logs" VALUES ('2', '1', 'mst_category', '2', 'insert', '2017-10-05 17:38:49');
INSERT INTO "public"."project_activity_logs" VALUES ('3', '1', 'mst_category', '3', 'insert', '2017-10-05 17:44:16');
INSERT INTO "public"."project_activity_logs" VALUES ('4', '1', 'mst_material', '1', 'insert', '2017-10-05 17:59:21');

-- ----------------------------
-- Table structure for project_audit_logs
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_audit_logs";
CREATE TABLE "public"."project_audit_logs" (
"id" int4 DEFAULT nextval('project_audit_logs_id_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"table_name" varchar(255) COLLATE "default",
"table_pk" int4 NOT NULL,
"column_name" varchar(255) COLLATE "default",
"old_value" varchar(1000) COLLATE "default",
"new_value" varchar(1000) COLLATE "default",
"action_dttime" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of project_audit_logs
-- ----------------------------

-- ----------------------------
-- Table structure for project_migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_migrations";
CREATE TABLE "public"."project_migrations" (
"version" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of project_migrations
-- ----------------------------
INSERT INTO "public"."project_migrations" VALUES ('18');

-- ----------------------------
-- Table structure for project_sessions
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_sessions";
CREATE TABLE "public"."project_sessions" (
"id" varchar(40) COLLATE "default" NOT NULL,
"ip_address" varchar(45) COLLATE "default" NOT NULL,
"timestamp" int4 NOT NULL,
"data" text COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of project_sessions
-- ----------------------------
INSERT INTO "public"."project_sessions" VALUES ('0257edbe632a3a25ec7c03348e021c86b06dfccd', '::1', '1507204799', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjA0NTU4O2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('09e8d76e50faf10cd355d52749b01b9bf08a2983', '::1', '1507201199', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAwOTQ4O2lkfGk6Mjt1c2VybmFtZXxzOjU6ImFkbWluIjtlbWFpbHxzOjEyOiJhZG1pbkBuby5jb20iO2xvZ2dlZGlufGI6MTs=');
INSERT INTO "public"."project_sessions" VALUES ('5572ccf5f1a8114a178e370fa905cfee8d8232cb', '::1', '1507203535', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAzNDgxO2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('60cd3f07bffb2cedc622208090d99e089fb8f421', '::1', '1507261977', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjYxOTc2Ow==');
INSERT INTO "public"."project_sessions" VALUES ('6bac44ea3dd5a9a22d138ce472bb3a03e3a2a84e', '::1', '1507204142', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAzODUzO2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('6effd5d46bf9f2d9d56d2f511f160c21495624d5', '::1', '1507202829', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAyNzM3O2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('6f70af9733e35fb457414b906e4702240a0d35b9', '::1', '1507202627', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAyMzI1O2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('77f1df59d539427e11a9583afb1934e943cba7de', '::1', '1507203466', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAzMTY2O2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('8d916c7ccb4a9dbbff959c43006f6b0c9a0feff1', '::1', '1507204506', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjA0MjE5O2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('d6247659145b2f80c90f0cd988b36a4cbe8e34f9', '::1', '1507200687', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAwNDg5O2lkfGk6Mjt1c2VybmFtZXxzOjU6ImFkbWluIjtlbWFpbHxzOjEyOiJhZG1pbkBuby5jb20iO2xvZ2dlZGlufGI6MTs=');
INSERT INTO "public"."project_sessions" VALUES ('e0c8d7969265738d7d1b82e579624aa4fadf19d2', '::1', '1507205663', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjA1NjAzO2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('e1521e1072fba444af3a6b215057aee45d9fa9bf', '::1', '1507202005', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAxODgwO2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('f000576847a4f912fa762231e8d89db20e9d6773', '::1', '1507201703', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjAxNDgzO2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('f322934d5272f12568f710b63e60bb32f5267a30', '::1', '1507205533', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjA1MjY3O2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');
INSERT INTO "public"."project_sessions" VALUES ('f8a060c4dcc2073d1b2fe555356ec926b10fac2a', '::1', '1507205012', 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTA3MjA0ODgxO2lkfGk6MTt1c2VybmFtZXxzOjEwOiJzdXBlcmFkbWluIjtlbWFpbHxzOjIwOiJzdXBlcmFkbWluQGdtYWlsLmNvbSI7bG9nZ2VkaW58YjoxOw==');

-- ----------------------------
-- View structure for view_group_permissions
-- ----------------------------
CREATE OR REPLACE VIEW "public"."view_group_permissions" AS 
 SELECT gp.group_id,
    gp.perm_id,
    perm.name AS permission,
    grp.name AS group_name
   FROM ((aauth_group_permissions gp
     JOIN aauth_groups grp ON ((grp.id = gp.group_id)))
     JOIN aauth_permissions perm ON ((perm.id = gp.perm_id)));

-- ----------------------------
-- View structure for view_material
-- ----------------------------
CREATE OR REPLACE VIEW "public"."view_material" AS 
 SELECT c.category,
    m.name,
    c.id
   FROM (mst_category c
     LEFT JOIN mst_material m ON ((c.id = m.category_id)));

-- ----------------------------
-- View structure for view_user_groups
-- ----------------------------
CREATE OR REPLACE VIEW "public"."view_user_groups" AS 
 SELECT ug.user_id,
    ug.group_id,
    g.name AS group_name,
    u.username,
    u.email,
    u.fullname
   FROM ((aauth_user_groups ug
     JOIN aauth_users u ON ((u.id = ug.user_id)))
     JOIN aauth_groups g ON ((g.id = ug.group_id)));

-- ----------------------------
-- View structure for view_user_permissions
-- ----------------------------
CREATE OR REPLACE VIEW "public"."view_user_permissions" AS 
 SELECT up.user_id,
    up.perm_id,
    perm.name AS permission,
    u.username,
    u.email,
    u.fullname
   FROM ((aauth_user_permissions up
     JOIN aauth_users u ON ((u.id = up.user_id)))
     JOIN aauth_permissions perm ON ((perm.id = up.perm_id)));

-- ----------------------------
-- View structure for view_users
-- ----------------------------
CREATE OR REPLACE VIEW "public"."view_users" AS 
 SELECT u.id,
    u.email,
    u.pass,
    u.username,
    u.fullname,
    u.banned,
    u.last_login,
    u.last_activity,
    u.date_created,
    u.forgot_exp,
    u.remember_time,
    u.remember_exp,
    u.verification_code,
    u.totp_secret,
    u.ip_address
   FROM aauth_users u;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."aauth_groups_id_seq" OWNED BY "aauth_groups"."id";
ALTER SEQUENCE "public"."aauth_login_attempts_id_seq" OWNED BY "aauth_login_attempts"."id";
ALTER SEQUENCE "public"."aauth_permissions_id_seq" OWNED BY "aauth_permissions"."id";
ALTER SEQUENCE "public"."aauth_pms_id_seq" OWNED BY "aauth_pms"."id";
ALTER SEQUENCE "public"."aauth_user_variables_id_seq" OWNED BY "aauth_user_variables"."id";
ALTER SEQUENCE "public"."aauth_users_id_seq" OWNED BY "aauth_users"."id";
ALTER SEQUENCE "public"."mst_category_id_seq" OWNED BY "mst_category"."id";
ALTER SEQUENCE "public"."mst_department_id_seq" OWNED BY "mst_department"."id";
ALTER SEQUENCE "public"."mst_machinery_category_id_seq" OWNED BY "mst_machinery_category"."id";
ALTER SEQUENCE "public"."mst_machinery_id_seq" OWNED BY "mst_machinery"."id";
ALTER SEQUENCE "public"."mst_material_id_seq" OWNED BY "mst_material"."id";
ALTER SEQUENCE "public"."mst_profile_id_seq" OWNED BY "mst_profile"."id";
ALTER SEQUENCE "public"."mst_scrap_id_seq" OWNED BY "mst_scrap"."id";
ALTER SEQUENCE "public"."project_activity_logs_id_seq" OWNED BY "project_activity_logs"."id";
ALTER SEQUENCE "public"."project_audit_logs_id_seq" OWNED BY "project_audit_logs"."id";

-- ----------------------------
-- Primary Key structure for table aauth_group_permissions
-- ----------------------------
ALTER TABLE "public"."aauth_group_permissions" ADD PRIMARY KEY ("perm_id", "group_id");

-- ----------------------------
-- Primary Key structure for table aauth_groups
-- ----------------------------
ALTER TABLE "public"."aauth_groups" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table aauth_login_attempts
-- ----------------------------
CREATE INDEX "aauth_login_attempts_id" ON "public"."aauth_login_attempts" USING btree ("id");

-- ----------------------------
-- Primary Key structure for table aauth_login_attempts
-- ----------------------------
ALTER TABLE "public"."aauth_login_attempts" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table aauth_permissions
-- ----------------------------
ALTER TABLE "public"."aauth_permissions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table aauth_pms
-- ----------------------------
CREATE INDEX "aauth_pms_id_sender_id_receiver_id_date_read" ON "public"."aauth_pms" USING btree ("id", "sender_id", "receiver_id", "date_read");

-- ----------------------------
-- Primary Key structure for table aauth_pms
-- ----------------------------
ALTER TABLE "public"."aauth_pms" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table aauth_sub_groups
-- ----------------------------
ALTER TABLE "public"."aauth_sub_groups" ADD PRIMARY KEY ("group_id", "subgroup_id");

-- ----------------------------
-- Primary Key structure for table aauth_user_groups
-- ----------------------------
ALTER TABLE "public"."aauth_user_groups" ADD PRIMARY KEY ("user_id", "group_id");

-- ----------------------------
-- Primary Key structure for table aauth_user_permissions
-- ----------------------------
ALTER TABLE "public"."aauth_user_permissions" ADD PRIMARY KEY ("perm_id", "user_id");

-- ----------------------------
-- Indexes structure for table aauth_user_variables
-- ----------------------------
CREATE INDEX "aauth_user_variables_user_id" ON "public"."aauth_user_variables" USING btree ("user_id");

-- ----------------------------
-- Primary Key structure for table aauth_user_variables
-- ----------------------------
ALTER TABLE "public"."aauth_user_variables" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table aauth_users
-- ----------------------------
ALTER TABLE "public"."aauth_users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_category
-- ----------------------------
ALTER TABLE "public"."mst_category" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_department
-- ----------------------------
ALTER TABLE "public"."mst_department" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_machinery
-- ----------------------------
ALTER TABLE "public"."mst_machinery" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_machinery_category
-- ----------------------------
ALTER TABLE "public"."mst_machinery_category" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_material
-- ----------------------------
ALTER TABLE "public"."mst_material" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_profile
-- ----------------------------
ALTER TABLE "public"."mst_profile" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_scrap
-- ----------------------------
ALTER TABLE "public"."mst_scrap" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table project_activity_logs
-- ----------------------------
ALTER TABLE "public"."project_activity_logs" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table project_audit_logs
-- ----------------------------
ALTER TABLE "public"."project_audit_logs" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table project_sessions
-- ----------------------------
ALTER TABLE "public"."project_sessions" ADD PRIMARY KEY ("id");
